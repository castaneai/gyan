package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/castaneai/gyan"
)

func main() {
	token := os.Getenv("GYAZO_ACCESS_TOKEN")
	if token == "" {
		log.Fatalf("missing env: GYAZO_ACCESS_TOKEN")
	}

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage: %s [options...] <filepath>\n\nOptions:\n", os.Args[0])
		flag.PrintDefaults()
	}

	var title, app, url, collection string
	flag.StringVar(&title, "title", "", "Image title")
	flag.StringVar(&app, "app", "gyan", "Application name")
	flag.StringVar(&url, "url", "", "referer URL")
	flag.StringVar(&collection, "collection", "", "Collection ID")
	var date int64
	flag.Int64Var(&date, "date", 0, "Datetime created (unixtime seconds)")
	flag.Parse()

	if date == 0 {
		date = time.Now().Unix()
	}

	inputPath := flag.Arg(0)
	if inputPath == "" {
		log.Fatalf("missing input filepath")
	}
	f, err := os.Open(inputPath)
	if err != nil {
		log.Fatalf("failed to open file '%s': %+v", inputPath, err)
	}
	defer f.Close()

	c := gyan.NewClient(token)
	ctx := context.Background()
	req := &gyan.UploadRequest{
		Title:        title,
		Filename:     "file",
		ImageData:    f,
		RefererURL:   url,
		Application:  app,
		CreatedAt:    time.Unix(date, 0),
		CollectionID: collection,
	}
	fmt.Fprintf(flag.CommandLine.Output(), "uploading to Gyazo: %+v...\n", req)
	resp, err := c.Upload(ctx, req)
	if err != nil {
		log.Fatalf("failed to upload to Gyazo: %+v", err)
	}
	fmt.Fprintln(os.Stdout, resp.PermalinkURL)
}
